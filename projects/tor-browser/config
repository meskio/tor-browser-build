# vim: filetype=yaml sw=2
version: '[% c("var/torbrowser_version") %]'
filename: 'tor-browser-[% c("version") %]-[% c("var/osname") %]-[% c("var/build_id") %]'

var:
  container:
    use_container: 1
  ddmg: '[% INCLUDE ddmg.sh %]'
  deps:
    - python
    - libparallel-forkmanager-perl
    - libfile-slurp-perl
    - zip
    - unzip
    - bzip2
    - xz-utils
    - patch

targets:
  linux-i686:
    var:
      mar_osname: linux32
      arch_deps:
        # Install libc6-i386 to be able to run 32bit mar tools (bug 29812)
        - libc6-i386
  linux-x86_64:
    var:
      mar_osname: linux64
  osx-x86_64:
    var:
      mar_osname: osx64
      arch_deps:
        - genisoimage
        - faketime
  windows:
    var:
      arch_deps:
        - python3-pefile
  windows-i686:
    var:
      mar_osname: win32
  windows-x86_64:
    var:
      mar_osname: win64
  android:
    build: '[% INCLUDE build.android %]'
    var:
      android_toolchain_version: 31.0.0
      verify_allowed_addons: 1

input_files:
  - project: container-image
  - filename: run_scripts
    enable: '[% ! c("var/android") %]'
  - project: firefox
    name: firefox
    enable: '[% ! c("var/android") %]'
  - project: fenix
    name: fenix
    enable: '[% c("var/android") %]'
  - project: tor
    name: tor
    enable: '[% ! c("var/android") %]'
  - project: firefox-langpacks
    name: firefox-langpacks
    enable: '[% ! c("var/testbuild") && ! c("var/android") %]'
  - project: https-everywhere
    name: https-everywhere
  - project: fonts
    name: fonts
    enable: '[% ! c("var/android") %]'
  - project: obfs4
    name: obfs4
    enable: '[% ! c("var/android") %]'
  - project: snowflake
    name: snowflake
    enable: '[% ! c("var/android") %]'
  - filename: Bundle-Data
    enable: '[% ! c("var/android") %]'
  - URL: https://addons.mozilla.org/firefox/downloads/file/3937112/noscript_security_suite-11.4.5-an+fx.xpi
    name: noscript
    sha256sum: 2a43901bfdb9250d30b805e77acd9a344ba557bc325ad6fc4f95c80cba21b840
  - filename: 'RelativeLink/start-tor-browser.desktop'
    enable: '[% c("var/linux") %]'
  - filename: 'RelativeLink/execdesktop'
    enable: '[% c("var/linux") %]'
  - filename: 'gtk3-settings.ini'
    enable: '[% c("var/linux") %]'
  - project: libdmg-hfsplus
    name: libdmg
    enable: '[% c("var/osx") %]'
  - project: nsis
    name: nsis
    enable: '[% c("var/windows") %]'
  - name: tbb-windows-installer
    project: tbb-windows-installer
    enable: '[% c("var/windows") %]'
  - filename: pe_checksum_fix.py
    enable: '[% c("var/windows") %]'
  # To generate a new keystore, see how-to-generate-keystore.txt
  - filename: android-qa.keystore
    enable: '[% c("var/android") %]'
  - name: '[% c("var/compiler") %]'
    project: '[% c("var/compiler") %]'
    enable: '[% c("var/android") %]'
  - name: electrum-nmc
    project: electrum-nmc
    enable: '[% c("var/namecoin") %]'
  - name: ncprop279
    project: ncprop279
    enable: '[% c("var/namecoin") %]'
  - name: stemns
    project: stemns
    enable: '[% c("var/namecoin") %]'
  - filename: namecoin.patch
    enable: '[% c("var/namecoin") %]'
  - filename: allowed_addons.json
    enable: '[% c("var/android") %]'
  - filename: verify_allowed_addons.py
    enable: '[% c("var/android") && c("var/verify_allowed_addons") %]'
  - filename: bridges_list.obfs4.txt
    enable: '[% ! c("var/android") %]'
  - filename: bridges_list.meek-azure.txt
    enable: '[% ! c("var/android") %]'
  - filename: bridges_list.snowflake.txt
    enable: '[% ! c("var/android") %]'
